package gw.acc.veridass.enhancement


enhancement PersonalVehicleEnhancement : entity.PersonalVehicle {

  /**
   * This property returns a Veridass action code for a given Vehicle based on the Job type.
   * If it is a PolicyChange, the Action code may vary depending on the changed entity.
  */
  property get VeridassActionCode() : VeridassActionCodes_Ext {
    switch(this.Branch.Job.Subtype) {
      case typekey.Job.TC_SUBMISSION:
          return VeridassActionCodes_Ext.TC_10
      case typekey.Job.TC_POLICYCHANGE:
          if(this.IsVeridassCorrectionFlag_Ext)
          {
            return VeridassActionCodes_Ext.TC_22
          }
          else if(this.Branch.PrimaryNamedInsured.DisplayName != this.Branch.BasedOn.PrimaryNamedInsured.DisplayName or isPolicyHolderChanged(this.Branch.PrimaryNamedInsured, this.Branch.BasedOn.PrimaryNamedInsured))
          {
            return VeridassActionCodes_Ext.TC_23
          }
          else {
            return VeridassActionCodes_Ext.TC_21
          }
      case typekey.Job.TC_REWRITE:
          return (this.Branch.BasedOn.Cancellation.CancelReasonCode == TC_SUSPENSION) ? VeridassActionCodes_Ext.TC_32 : VeridassActionCodes_Ext.TC_11
      case typekey.Job.TC_REINSTATEMENT:
          return VeridassActionCodes_Ext.TC_11
      case typekey.Job.TC_CANCELLATION:
          return (this.Branch.EditEffectiveDate.compareTo(this.Branch.PeriodStart) == 0) ? VeridassActionCodes_Ext.TC_33 : VeridassActionCodes_Ext.TC_31
    }

    return null
  }

  private function isPolicyHolderChanged(policyHolder : PolicyPriNamedInsured, basedOnPolicyHolder : PolicyPriNamedInsured) : boolean {
    var address    = policyHolder.ContactDenorm.PrimaryAddress
    var oldAddress = basedOnPolicyHolder.ContactDenorm.PrimaryAddress
    return address.AddressLine1 != oldAddress.AddressLine1 or address.AddressLine2 != oldAddress.AddressLine2 or address.AddressLine3 != oldAddress.AddressLine3
  }

}
