package gw.acc.veridass.builder

uses java.lang.StringBuilder
uses java.lang.System
uses java.math.BigDecimal
uses java.text.SimpleDateFormat
uses java.util.Date

class VeridassFile {

  private var _recordFile   : StringBuilder as RecordFile

  public var _counter       : BigDecimal as VeridassCounter = 0000
  public var _numberOfLines : int                           = 0

  construct() {
    _recordFile = new StringBuilder()
  }

  public function appendLine(line : String) {
     _recordFile.append(line).append(System.lineSeparator())
     _numberOfLines++
  }

  public function incrementCounter() {
    _counter++
  }

  public function resetCounter() {
    _counter       = 0000
    _numberOfLines = 0
  }

  public function populateTerminationLine(startDate: Date, endDate: Date, counterOfFiles: int)  {
    var dateFormat = new SimpleDateFormat("ddMMyyyy")

    var termination = new VeridassTerminationRecord() {
      :NBBCode         = ScriptParameters.NBB_Code as String,
      :Blank1          = null,
      :ActionCode      = VeridassActionCodes_Ext.TC_99.Code as int,
      :StartDate       = dateFormat.format(startDate),
      :EndDate         = dateFormat.format(endDate),
      :NumberOfRecords = _numberOfLines as String,
      :Blank2          = null,
      :TemplateVersion = ScriptParameters.Veridass_Termination_Template.toString(),
      :CounterOfFiles  = counterOfFiles as String
    }

    _recordFile.append(termination.createLine(termination)).append(System.lineSeparator())
  }
}