package gw.api.name

@Export
class ContactNameDelegate implements ContactNameFields {
  private var _contact : Contact

  construct(contact : Contact) {
    _contact = contact
  }
  
  override property get Name() : String {
    return _contact.Name
  }

  override property set Name(value : String) {
    _contact.Name = value
  }

  override property get NameKanji() : String {
    return _contact.NameKanji
  }

  override property set NameKanji(value: String) {
    _contact.NameKanji = value
  }

  override property get Prefix(): NamePrefix {
    if(_contact typeis Company) return _contact.Prefix_Ext
    else if(_contact typeis Person) return _contact.Prefix
    else return null
  }

  override property set Prefix(value: NamePrefix) {
    if(_contact typeis Company) {
      _contact.Prefix_Ext = value
    } else if(_contact typeis Person) {
      _contact.Prefix = value
    } else {
      // Do nothing. Included for clarity
    }
  }
}
