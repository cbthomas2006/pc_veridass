package gw.acc.veridass.mapper


enum VeridassVehicleCodesEnum_Ext {

  VehicleCode_1(1), VehicleCode_4(4)

  private var _numbering : int as Numbering
  private construct(n: int) {
    this._numbering = n
  }

}