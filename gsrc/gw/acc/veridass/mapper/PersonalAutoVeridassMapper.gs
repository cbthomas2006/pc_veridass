package gw.acc.veridass.mapper

uses gw.acc.veridass.builder.VeridassFile
uses gw.acc.veridass.builder.VeridassRecord
uses gw.api.util.DateUtil
uses gw.api.util.TypecodeMapperUtil

uses java.text.DecimalFormat
uses java.text.SimpleDateFormat
uses java.util.ArrayList

/**
 * A Veridass Mapper for Personal Auto. This class creates a Veridass Record for each Policy Period, or two
 * in case of a combined transaction.
*/
class PersonalAutoVeridassMapper implements VeridassMapper<PersonalAutoLine> {

  // Instance of Singleton
  private static var _instance : PersonalAutoVeridassMapper = null

  // Zero format for Date
  private static final var _ZEROED_DATE_FORMAT  = "000000"
  // Blank String
  private static final var _BLANK = ""

  private var _typecodeMapper = TypecodeMapperUtil.getTypecodeMapper()
  private var _veridassFile : VeridassFile = null

  //Allowed action codes for a Start date
  private final var _mandatoryCodesForStartDate = {VeridassActionCodes_Ext.TC_10, VeridassActionCodes_Ext.TC_11, VeridassActionCodes_Ext.TC_21, VeridassActionCodes_Ext.TC_23}
  // Zeroes the start date
  private final var _zeroedCodesForStartDate    = {VeridassActionCodes_Ext.TC_31, VeridassActionCodes_Ext.TC_33}

  //Allowed action codes for an End date
  private final var _mandatoryCodesForEndDate   = {VeridassActionCodes_Ext.TC_10, VeridassActionCodes_Ext.TC_31, VeridassActionCodes_Ext.TC_32, VeridassActionCodes_Ext.TC_33}
  // Zeroes the start date
  private final var _zeroedCodesForEndDate      = {VeridassActionCodes_Ext.TC_10, VeridassActionCodes_Ext.TC_11, VeridassActionCodes_Ext.TC_21, VeridassActionCodes_Ext.TC_23}

  //No Commercial Vehicle bit flag
  private final var _NoCommercialVehicleFlag    = "2" // do it as typelist


  construct(file : VeridassFile) {
    _veridassFile = file
  }

  //Singleton instance
  public static function getInstance(file : VeridassFile) : PersonalAutoVeridassMapper {
    if(_instance == null) {
      return new PersonalAutoVeridassMapper(file)
    }
    return _instance
  }

  override function mapFromPolicyPeriod(period: PolicyPeriod): List<VeridassRecord> {

    var records = new ArrayList<VeridassRecord>()

    //Looping all the Vehicles for the Policy Period
    period.PersonalAutoLine.Vehicles.each( \ vehicle -> {

      //If the License Plate has changed and it is not a Veridass correction, a combined transaction 33+21 has to be sent
      if(vehicle.isFieldChangedFromBasedOn("LicensePlate") and not vehicle.IsVeridassCorrectionFlag_Ext)
      {
        records.add(populateRecord(period, vehicle.BasedOn, VeridassActionCodes_Ext.TC_33.Code, true))
        records.add(populateRecord(period, vehicle, vehicle.VeridassActionCode.Code, false))
      }
      else
      {
        //Add the usual Veridass record for the given period
        records.add(populateRecord(period, vehicle, vehicle.VeridassActionCode.Code, false))
      }

    })

    //In case of Policy Change, check if any vehicle has been removed. If so, add a record for the removed vehicle too, with a 31 code
    if(period.Job typeis PolicyChange)
    {

      var vehicles        = period.PersonalAutoLine.Vehicles
      var oldVehicles     = period.BasedOn.PersonalAutoLine.Vehicles
      var removedVehicles = oldVehicles.where( \ v -> not vehicles.hasMatch( \ veh -> veh.VehicleNumber == v.VehicleNumber))
      removedVehicles.each( \ v -> {
        records.add(populateRecord(period.BasedOn, v, VeridassActionCodes_Ext.TC_31.Code, false))
      })

    }

    return records
  }

  private function populateRecord(period: PolicyPeriod, vehicle: PersonalVehicle, actionCode: String, isCombined: boolean): VeridassRecord {
    var dateFormat = new SimpleDateFormat("ddMMyyyy")
    var record     = new VeridassRecord()

    //Policy related fields
    record.NBBCode              = ScriptParameters.NBB_Code as String
    record.PolicyNumber         = period.PolicyNumber
    record.BranchNumber         = ScriptParameters.BranchNumber as int
    record.CommercialVehicle    = _NoCommercialVehicleFlag
    record.ActionCode           = actionCode as int

    final var veridassCode      = VeridassActionCodes_Ext.get(actionCode)

    //Populate vehicle start and date only if needed for the given action code
    if(_mandatoryCodesForStartDate.contains(veridassCode) and not vehicle.IsVeridassCorrectionFlag_Ext) {
      record.EffectiveDate = dateFormat.format(vehicle.EffectiveDate)
    }
    else if(_zeroedCodesForStartDate.contains(veridassCode)) {
      record.EffectiveDate = _ZEROED_DATE_FORMAT
    }

    if(_mandatoryCodesForEndDate.contains(veridassCode) and not vehicle.IsVeridassCorrectionFlag_Ext) {
      record.EndDate = dateFormat.format(vehicle.ExpirationDate)
    }
    else if(_zeroedCodesForEndDate.contains(veridassCode)) {
      record.EndDate = _ZEROED_DATE_FORMAT
    }

    //Vehicle related fields
    record.LicensePlate = vehicle.LicensePlate
    record.VinNumber    = vehicle.Vin

    //Must be 01 for TATV but in case of VeridassActionCode.TC_22 is blank
    record.Unifier      = (veridassCode == VeridassActionCodes_Ext.TC_22) ? _BLANK : ScriptParameters.Unifier as String

    record.VehicleCode  = (vehicle.IsVeridassCorrectionFlag_Ext) ? VeridassVehicleCodesEnum_Ext.VehicleCode_4.Numbering : VeridassVehicleCodesEnum_Ext.VehicleCode_1.Numbering
    record.MakeAndModel = "${vehicle.Make} ${vehicle.Model}"

    //For Personal Auto Line this will be zero as it's only valued onto Commercial Line
    record.BBAACategory = mapBBAACategory(:vehicle = vehicle)

    /**
     * DIV Interaction - car@test accelerator integration here
     *
     * DIV Category, CubicCapacity, Power, PVANumber, WVTANumber, WVTAVariation, WVTAVersion
     */
    var divAdapter = new DIVAdapterMapper()
    divAdapter.mapDIVDetailsOntoVeridassRecord(:veridassRecord = record, :vehicle = vehicle)

    //Policyholder fields and Address fields
    mapPolicyHolderAndAddress(:veridassMarker = period.VeridassMarker_Ext, :record = record)

    record.LanguageCode     = _typecodeMapper.getAliasByInternalCode("LanguageType", "Veridass:language", period.PrimaryNamedInsured.AccountContactRole.AccountContact.Account.PrimaryLanguage.Code)
    record.DateOfUpdate     = dateFormat.format(DateUtil.currentDate())

    //The HourOfUpdate field is a line counter in the Veridass File. If two lines are combined, the same number must be used
    record.HourOfUpdate     = formatHourOfUpdate(isCombined)
    return record
  }

  //Map of PolicyHolder and Address fields
  private function mapPolicyHolderAndAddress(veridassMarker: VeridassMarker_Ext, record: VeridassRecord) {
    //Policyholder fields
    record.FamilyName       = veridassMarker.FamilyName_Ext
    record.FirstName        = veridassMarker.FirstName_Ext

    final var namePrefix    = veridassMarker.Prefix_Ext
    record.TitleCode        = namePrefix != null ? _typecodeMapper.getAliasByInternalCode("NamePrefix", "Veridass:namePrefix", namePrefix.Code).toInt() : 0
    record.TitleDescription = namePrefix != null ? namePrefix.DisplayName : null

    //Address fields
    record.Street           = veridassMarker.Street_Ext
    record.HouseNumber      = veridassMarker.HouseNumber_Ext
    record.BoxNumber        = veridassMarker.BoxNumber_Ext
    record.ZipCode          = veridassMarker.ZipCode_Ext
    record.City             = veridassMarker.City_Ext
    record.CountryCode      = veridassMarker.Country_Ext
  }

  // Title
  private function getPrefix(policyHolder: entity.PolicyPriNamedInsured): NamePrefix {
    var contact = policyHolder.ContactDenorm

    var namePrefix: NamePrefix = null
    if(contact typeis Person) {
      namePrefix = contact.Prefix
    }
    else if(contact typeis Company) {
      namePrefix = contact.Prefix_Ext
    }
    return namePrefix
  }

  // BBAACategory
  private function mapBBAACategory(vehicle: PersonalVehicle): String {
    return vehicle.BodyType?.BBAAVehicleCategory_Ext.Code
  }

  //The HourOfUpdate field is a line counter in the Veridass File. If two lines are combined, the same number must be used
  private function formatHourOfUpdate(isCombined: boolean): String {
    var df = new DecimalFormat("0000")

    if (isCombined) {
      return df.format(_veridassFile.VeridassCounter)
    }
    else {
      _veridassFile.incrementCounter()
      return df.format(_veridassFile.VeridassCounter)
    }
  }

}