package gw.acc.veridass.builder

/**
 * @author: Guido Esposito
 */
class VeridassTerminationRecord extends VeridassRecordBase{

  @VeridassData(5,1,true)
  private var _nbbCode : String           as NBBCode

  @VeridassData(15,6,true)
  private var _blank1 : String            as Blank1

  @VeridassData(2,21,true)
  private var _actionCode : int           as ActionCode

  @VeridassData(8,23,true)
  private var _startDate : String         as StartDate

  @VeridassData(8,31,true)
  private var _endDate : String           as EndDate

  @VeridassData(9,39,true)
  private var _numberOfRecords : String   as NumberOfRecords

  @VeridassData(18,48,true)
  private var _blank2 : String            as Blank2

  @VeridassData(20,66,true)
  private var _templateVersion : String   as TemplateVersion

  @VeridassData(9,86,true)
  private var _counterOfFiles : String    as CounterOfFiles

}