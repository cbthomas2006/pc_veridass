package gw.acc.veridass.util

/**
 * A String Util class with static util functions for the Veridass file creation
*/
class StringUtil {

  /**
  * This function adds blank spaces in case an alphanumeric string is shorter than the expected length.
  * For Veridass, all the Alphanumeric strings must be left-aligned and filled with spaces.
  *
  * If the String is numeric, it adds zeros before the actual number, as Numbers must be right-aligned.
  *
  */
  @Param("s","The String to be modified")
  @Param("len","The expected length of the String")
  @Returns("A modified String")
  static public function getString(s : String, len : int) : String {
    if(s == null) {
      return (s.Numeric) ? getZeros(len) : getSpaces(len)
    }
    if (s.length == len) {
      return s
    }
    if (s.length < len) {
      return (s.Numeric) ? (getZeros(len - s.length) + s) : (s + getSpaces(len - s.length))
    }
    return s.substring(0, len)
  }

  //Create a blank string of a given length
  @Param("n","the lenght of the string")
  @Returns("A blank string with n chars")
  static public function getSpaces(n: int): String {
    return new String(new char[n]).replaceAll('\0', ' ');
  }

  //Create a string with a given number of zeros
  @Param("n","the lenght of the string")
  @Returns("A blank string with n zeros")
  static public function getZeros(n : int) : String {
    return new String(new char[n]).replaceAll('\0', '0');
  }


}