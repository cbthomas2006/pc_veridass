package gw.acc.veridass.preupdate

uses gw.api.preupdate.PreUpdateContext

uses java.util.concurrent.locks.ReentrantLock

/**
 *-----------------------------------------------------------------------------
 * Management of PolicyPeriod.VeridassMaker_Ext property need for Veridass integration
 *
 *-----------------------------------------------------------------------------
 * Initial Developer: gesposito
 * Date: (21/01/2015)
 *-----------------------------------------------------------------------------
 *
 * Please refer to version control for modifications log.
 *
 */
class VeridassMarkerPreUpdateHooker{

  static final var _instance : VeridassMarkerPreUpdateHooker as readonly Instance = new VeridassMarkerPreUpdateHooker()

  private static var _lock = new ReentrantLock()

  private construct() {}

  /**
   * update the Veridass Marker belonging to a PolicyPeriod
   */
  @Param("context", "The PreUpdateContext")
  public function createVeridassMarker(context : PreUpdateContext) {
    var effDated = context.InsertedBeans.firstWhere( \ elt -> elt typeis EffDated) as EffDated
    if (effDated == null) {
      effDated = context.UpdatedBeans.firstWhere( \ elt -> elt typeis EffDated) as EffDated
    }
    if (effDated == null) {
      effDated = context.RemovedBeans.firstWhere( \ elt -> elt typeis EffDated) as EffDated
    }

    if (effDated != null and effDated.BranchUntyped typeis PolicyPeriod) {
      makeRecord(:policyPeriod = effDated.BranchUntyped)
      return
    }
  }

  // create the record VeridassMarker_Ext
  private function makeRecord(policyPeriod: PolicyPeriod) {
    final var policyHolder = policyPeriod.PrimaryNamedInsured
    final var firstName    = policyHolder typeis Company ? policyHolder.Name : policyHolder.FirstName
    final var familyName   = policyHolder typeis Company ? null              : policyHolder.LastName
    final var address      = policyHolder.ContactDenorm.PrimaryAddress

    // creating new VeridassMarker_Ext and associating to the instance of PolicyPeriod
    policyPeriod.VeridassMarker_Ext = new()
    {
      :Sent             = false,
      :FamilyName_Ext   = familyName,
      :FirstName_Ext    = firstName,
      :Prefix_Ext       = getPrefix(policyHolder),
      :Street_Ext       = address.AddressLine1,
      :HouseNumber_Ext  = address.AddressLine2,
      :BoxNumber_Ext    = address.AddressLine3,
      :ZipCode_Ext      = address.PostalCode,
      :City_Ext         = address.City,
      :Country_Ext      = address.Country.Code
    }
  }

  // Title
  private function getPrefix(policyHolder: PolicyPriNamedInsured): NamePrefix {
    var contact = policyHolder.ContactDenorm

    var namePrefix: NamePrefix = null
    if(contact typeis Person) {
      namePrefix = contact.Prefix
    }
    else if(contact typeis Company) {
      namePrefix = contact.Prefix_Ext
    }
    return namePrefix
  }

}