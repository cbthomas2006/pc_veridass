package gw.acc.veridass.mapper

uses gw.acc.veridass.builder.VeridassRecord

/**
 * @author: Guido Esposito
 *
 * This class is the Interceptor class to make the interaction between Veridass and DIV integrations
*/
class DIVAdapterMapper<T extends VeridassRecord> {

  public function mapDIVDetailsOntoVeridassRecord(veridassRecord: T, vehicle: PersonalVehicle) {
    // retrieving DIV information
    var div = retrieveDIV( vehicle )

    // mapping between DIV info and Veridass record
    veridassRecord.DIVCategory   = div.DIVCategory
    veridassRecord.CubicCapacity = div.CubicCapacity
    veridassRecord.Power         = div.Power
    veridassRecord.PVANumber     = div.PVANumber
    veridassRecord.WVTANumber    = div.WVTANumber
    veridassRecord.WVTAVariation = div.WVTAVariation
    veridassRecord.WVTAVersion   = div.WVTAVersion
  }

  /** derive DIV info from vehicle **/
  private function retrieveDIV(vehicle: PersonalVehicle): DIV {
    final var bodyType = vehicle.BodyType

    // enrich this VO with information from DIV
    return new DIV() {
      :DIVCategory = bodyType.Code
    }

  }

  // DIV internal VO
  private class DIV {

    private var _divCategory  : String      as DIVCategory
    private var _cubicCapacity : int        as CubicCapacity
    private var _power : int                as Power
    private var _pvaNumber : String         as PVANumber
    private var _wvtaNumber : String        as WVTANumber
    private var _wvtaVariation : String     as WVTAVariation
    private var _wvtaVersion : String       as WVTAVersion

  }

}