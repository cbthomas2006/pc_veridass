package gw.acc.veridass.mapper

uses gw.acc.veridass.builder.VeridassRecord

interface VeridassMapper<L extends PolicyLine> {

  public function mapFromPolicyPeriod(period : PolicyPeriod) : List<VeridassRecord>

}