package gw.acc.veridass.builder

/**
 *
 * This annotation class defines the annotation used by a Veridass Record
 *
 */
class VeridassData implements IAnnotation {

  private var _length      : int     as readonly Length
  private var _position    : int     as readonly Position
  private var _isMandatory : boolean as readonly IsMandatory

  @Param("stringLength","The length of the Veridass record item")
  @Param("position","The position of an item in a Veridass line")
  construct(stringLength : int, position : int) {
    _length   = stringLength
    _position = position
  }

  @Param("stringLength","The length of the Veridass record item")
  @Param("position","The position of an item in a Veridass line")
  @Param("isMandatory","Boolean flag to check whether the item is mandatory or not")
  construct(stringLength : int, position : int, isMandatory : boolean) {
    _length      = stringLength
    _position    = position
    _isMandatory = isMandatory
  }

  override public function toString() : String {
    return "VeridassData - Length ${Length}, Position ${Position}, IsMandatory ${IsMandatory}"
  }

}