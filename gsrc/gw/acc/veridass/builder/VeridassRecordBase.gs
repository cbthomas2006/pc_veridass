package gw.acc.veridass.builder

uses gw.acc.veridass.util.StringUtil
uses gw.lang.reflect.TypeSystem

uses java.lang.IllegalArgumentException
uses java.lang.StringBuilder


class VeridassRecordBase<T> {

  private final var _ThrowsExceptions = false

  public function createLine(record : T): String {

    var properties   = TypeSystem.get(record.Class).TypeInfo.Properties
    var recordString = new StringBuilder()

    properties.each( \ prop -> {
      if(not prop.hasAnnotation(VeridassData))  {
        return
      }

      var veridassData = prop.getAnnotation(VeridassData).Instance as VeridassData
      var value        = prop.Accessor?.getValue(record)?.toString()

      //TODO - put back the exception when all the compulsory fields have been set properly
      if(_ThrowsExceptions) {
        if(veridassData.IsMandatory and value == null) {
          throw new IllegalArgumentException("The property ${prop.DisplayName} is mandatory and cannot be null.")
        }
      }

      value = StringUtil.getString(value, veridassData.Length)
      recordString.append(value)
    })

    return recordString as String

  }


}