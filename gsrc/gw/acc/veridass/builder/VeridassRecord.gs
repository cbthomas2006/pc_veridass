package gw.acc.veridass.builder

uses java.util.Date

/**
 *
 * Veridass Record file. All the fields use a VeridassData annotation with length, position and an isMandatory flag.
 *
 */
class VeridassRecord extends VeridassRecordBase {

  @VeridassData(5,1,true)
  private var _nbbCode : String           as NBBCode

  @VeridassData(12,6,true)
  private var _policyNumber : String      as PolicyNumber

  @VeridassData(2,18,false)
  private var _branchNumber : int         as BranchNumber

  @VeridassData(1,20,true)
  private var _commercialVehicle : String as CommercialVehicle

  @VeridassData(2,21,true)
  private var _actionCode : int           as ActionCode

  @VeridassData(8,23,true)
  private var _effectiveDate : String     as EffectiveDate

  @VeridassData(8,31,true)
  private var _endDate : String           as EndDate

  @VeridassData(9,39,true)
  private var _licensePlate : String      as LicensePlate

  @VeridassData(17,48,true)
  private var _vinNumber : String         as VinNumber

  @VeridassData(2,65,false)
  private var _unifier : String           as Unifier

  @VeridassData(1,67,true)
  private var _vehicleCode : int          as VehicleCode

  @VeridassData(60,68,true)
  private var _makeAndModel : String      as MakeAndModel

  @VeridassData(1,128,true)
  private var _bbaaCategory : String      as BBAACategory

  @VeridassData(2,129,true)
  private var _divCategory  : String      as DIVCategory

  @VeridassData(5,131,false)
  private var _cubicCapacity : int        as CubicCapacity

  @VeridassData(5,136,false)
  private var _power : int                as Power

  @VeridassData(6,141,false)
  private var _pvaNumber : String         as PVANumber

  @VeridassData(40,147,false)
  private var _wvtaNumber : String        as WVTANumber

  @VeridassData(25,187,false)
  private var _wvtaVariation : String     as WVTAVariation

  @VeridassData(35,212,false)
  private var _wvtaVersion : String       as WVTAVersion

  @VeridassData(35,247,true)
  private var _familyName : String        as FamilyName

  @VeridassData(15,282,false)
  private var _firstName : String         as FirstName

  @VeridassData(1,297,false)
  private var _titleCode : int            as TitleCode

  @VeridassData(17,298,false)
  private var _titleDescription : String  as TitleDescription

  @VeridassData(30,315,true)
  private var _street : String            as Street

  @VeridassData(5,345,false)
  private var _houseNumber : String       as HouseNumber

  @VeridassData(4,350,false)
  private var _boxNumber : String         as BoxNumber

  @VeridassData(7,354,true)
  private var _zipCode : String           as ZipCode

  @VeridassData(24,361,true)
  private var _city : String              as City

  @VeridassData(3,385,false)
  private var _countryCode : String       as CountryCode

  @VeridassData(1,388,true)
  private var _languageCode : String      as LanguageCode

  @VeridassData(8,389,true)
  private var _dateOfUpdate : String      as DateOfUpdate

  @VeridassData(4,397,true)
  private var _hourOfUpdate : String      as HourOfUpdate


}