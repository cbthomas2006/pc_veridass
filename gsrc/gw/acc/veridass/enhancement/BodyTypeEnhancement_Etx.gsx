package gw.acc.veridass.enhancement


/**
 * @author: Guido Esposito
 */
enhancement BodyTypeEnhancement_Etx : typekey.BodyType {

  @Returns("Returns the BBAAVehicleCategory_Ext equivalent to the BodyType instance")
  public property get BBAAVehicleCategory_Ext(): BBAAVehicleCategory_Ext {
    return this.Categories.firstWhere( \ cat -> cat.IntrinsicType.TypeInfo.Name == typekey.BBAAVehicleCategory_Ext.Type.TypeInfo.Name) as BBAAVehicleCategory_Ext
  }

}
