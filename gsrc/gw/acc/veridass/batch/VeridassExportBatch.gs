package gw.acc.veridass.batch

uses gw.acc.veridass.builder.VeridassFile
uses gw.api.database.IQueryBeanResult
uses gw.api.database.Query
uses gw.api.util.DateUtil
uses gw.pl.logging.LoggerCategory
uses gw.processes.BatchProcessBase

uses java.io.File
uses java.io.FileWriter
uses java.io.Writer
uses java.lang.Exception
uses java.lang.StringBuilder
uses java.text.SimpleDateFormat
uses java.util.Date

/**
 *
 * A batch process to query all the Policy Periods to send to Veridass and to create the Veridass flat file
 *
 */
class VeridassExportBatch extends BatchProcessBase {

  private static final var _log = LoggerCategory.INTEGRATION

  private final var _REPORT_FILE_NAME        = "Veridass_"
  private final var _REPORT_FILE_DATE_FORMAT = "yyyyMMddHHmm"
  private final var _REPORT_FILE_EXT         = ".txt"

  // this user is to used to commit the bundle.
  private final var _USER_COMMITTING_TX      = "su" // Please change it when you are going to deploy into a PC, with an appropriate one

  construct() {
    super(BatchProcessType.TC_VERIDASSEXPORT)
  }

  override function doWork() {

    createParentFolderIfNeeded()

    //Create the File Writer
    var writer = new FileWriter(FilePath)

    gw.transaction.Transaction.runWithNewBundle(\bundle -> {

      var period : PolicyPeriod = null
      try {

        // Create the Veridass File object
        var veridassFile = new VeridassFile()

        // Query all the valid Policies
        var validPolicies = getPolicyPeriodToBeElaborated(:modulateBusinessDays = 1)

        validPolicies.each( \ pp -> {
          // Slicing the PolicyPeriod
          period = pp.getSlice(pp.EditEffectiveDate)

          // looping on the lines
          period.Lines.where( \ l -> l.createVeridassMapper(veridassFile) != null).each( \ line -> {

            //The Veridass Mapper
            var mapper  = line.createVeridassMapper(veridassFile)
            var records = mapper.mapFromPolicyPeriod(period)
            _log.info("${BatchProcessType.TC_VERIDASSEXPORT} - Found ${records.size()} records to be elaborated")

            records.each( \ r -> {
              // creating the mapped record as line within the buffer
              var value = r.createLine(r)
              _log.info("Record (Policy: ${r.PolicyNumber}, Branch: ${r.BranchNumber})--> ${value}")
              veridassFile.appendLine(value)
            })

            var marker  = bundle.add( VeridassMarker_Ext.findBy(:publicId = period.VeridassMarker_Ext.PublicID) )
            marker.Sent = true

          })

        })

        //Set the termination line
        final var earliestStartDate = validPolicies.first().PeriodStart
        veridassFile.populateTerminationLine(:startDate = earliestStartDate, :endDate = DateUtil.currentDate(), :counterOfFiles = NumberOFExecutions)

        //Writ out the Report onto the FilePath
        writeOutVeridassReport(:veridassFile = veridassFile, :writer = writer)

        veridassFile.resetCounter()
        incrementOperationsCompleted()
      }
      catch(ex : Exception) {
        _log.error("An error has been occurred during exectuing of ${BatchProcessType.TC_VERIDASSEXPORT} batch process", ex)
        incrementOperationsFailed()
        OperationsFailedReasons.add(ex.Message)
        throw ex
      }
      finally{
        writer.close()
      }

    }, _USER_COMMITTING_TX)

  }

  private function getPolicyPeriodToBeElaborated(modulateBusinessDays: int): IQueryBeanResult<PolicyPeriod> {
    var lastMonth = Date.CurrentDate.addMonths(-1)

    var validPolicies = Query.make(PolicyPeriod)
        .compare(PolicyPeriod#VeridassMarker_Ext, NotEquals, null)
        .compare(PolicyPeriod#Status, Equals, PolicyPeriodStatus.TC_BOUND)
        //.between(PolicyPeriod#EditEffectiveDate, lastMonth, DateTime.Today.addBusinessDays( modulateBusinessDays ))
        .join(PolicyPeriod#VeridassMarker_Ext)
        .compare(VeridassMarker_Ext#Sent, Equals, false)
        .select()

    validPolicies.orderBy( \ row -> row.PeriodStart)

    return validPolicies
  }

  private function createParentFolderIfNeeded() {
    final var file = new File(FilePath)
    final var parent_directory = file.getParentFile()

    if (null != parent_directory) {
      parent_directory.mkdirs()
    }
  }

  private function writeOutVeridassReport(veridassFile: VeridassFile, writer: Writer) {
    _log.info("Writing the content of the Veridass File onto ${FilePath} path... ")
    final var out = veridassFile.RecordFile.toString()
    writer.write(out)
    _log.debug("Written ${out.size} bytes")

    //move this line in debug for future development
    _log.info("The content of the file will be --> \n ${out}")
  }

  /**
   * The FilePath Property
   */
  private property get FilePath(): String {
    var path = new StringBuilder(ScriptParameters.VeridassExportFilePath).append(File.separator)
    path.append(_REPORT_FILE_NAME)
        .append(new SimpleDateFormat(_REPORT_FILE_DATE_FORMAT).format(DateUtil.currentDate()))
        .append(_REPORT_FILE_EXT)
    return path.toString()
  }

  private property get NumberOFExecutions(): int {
    var info  = new gw.api.web.tools.BatchProcessInfoPage()
    var stats = info.findProcessHistory( BatchProcessType.TC_VERIDASSEXPORT )
    return stats.Count
  }

}
