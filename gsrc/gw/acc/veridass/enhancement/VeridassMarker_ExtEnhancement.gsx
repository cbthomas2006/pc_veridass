package gw.acc.veridass.enhancement

uses gw.api.database.Query

enhancement VeridassMarker_ExtEnhancement : entity.VeridassMarker_Ext {

  public static function findBy(final publicId: String): VeridassMarker_Ext {
    return Query.make(VeridassMarker_Ext).compare(VeridassMarker_Ext#PublicID, Equals, publicId).select().AtMostOneRow
  }

}
